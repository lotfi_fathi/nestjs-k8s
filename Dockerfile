# ---------Build stage--------------
# Base image
FROM node:14-alpine AS build

# Create app directory
WORKDIR /app

# copy app dependencies
COPY package*.json ./

# use npm cache
# RUN npm config set cache /app/.npm-cache

# install app dependencies
RUN npm install

# Copy app source code
COPY . .

# Build app
RUN npm run build

# --------------Production stage------------
FROM node:14-alpine
WORKDIR /app
COPY package*.json ./
RUN npm install --only=production
COPY --from=build /app/dist ./dist

# Start the app
CMD ["node", "dist/main.js"]